# What's this

This is a daily built OCI(docker) image of [rustfmt](https://github.com/rust-lang-nursery/rustfmt). It is built (and pushed if it succeds) automaticly every day at 6am UTC.

It's based on the [rust nightly image](https://github.com/rust-lang-nursery/docker-rust-nightly).

## Usage

Run rustfmt on your crate:

`docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/myapp -w /usr/src/myapp registry.gitlab.com/alatiera/rustfmt-oci-image/rustfmt:nightly`

Print the diff, no override:

`docker run --rm --user "$(id -u)":"$(id -g)" -v "$PWD":/usr/src/myapp -w /usr/src/myapp registry.gitlab.com/alatiera/rustfmt-oci-image/rustfmt:nightly cargo fmt --all -- --write-mode=diff`

## Using rustfmt with the gitlab-ci

Gitlab-CI template

```yml
# Exits and builds fails if on bad format
rustfmt:
  image: "registry.gitlab.com/alatiera/rustfmt-oci-image/rustfmt:nightly"
  script:
  - rustc --version && cargo --version
  - cargo fmt --all -- --write-mode=diff
```
